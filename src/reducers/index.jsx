import { combineReducers } from 'redux';
import exampleReducer from './reducer_listContent.jsx';
import currentListType from './reducer_currentListType.jsx';

const rootReducer = combineReducers({
  current: currentListType,
  list: exampleReducer
});

export default rootReducer;