import { CHANGE_LIST } from '../actions'


export default function(state = [], action){
  switch (action.type) {
    case CHANGE_LIST:
      if(action.payload == "second-names") {
        return [
          "Nazar",
          "George",
          "Eugene",
          "Olga"
        ]
      }else if (action.payload == "names"){
        return [
          "Starantsov Nazar",
          "Starantsov George",
          "Starantsov Eugene",
          "Starantsova Olga"
        ]
      }
  }
  
  return [
    "Nazar",
    "George",
    "Eugene",
    "Olga"
  ]
}