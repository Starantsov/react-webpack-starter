import React, { Component } from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { changeListState } from '../actions';

class List extends Component {
  constructor(props) {
    super(props);
    this.onButtonClick = this.onButtonClick.bind(this);
  }
  onButtonClick(event) {
    this.props.changeListState(this.props.currentState);
  }
  render() {
    const list = this.props.list.map( elem => <li key={elem}>{elem}</li> )
    return (
      <section>
        <ul>
          { list }
        </ul>
        <button type="button" onClick={this.onButtonClick}>See second names</button>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    list: state.list,
    currentState: state.current 

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    changeListState: changeListState,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(List);