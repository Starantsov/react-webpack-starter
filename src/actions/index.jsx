//Action types
export const CHANGE_LIST = "CHANGE_LIST";

//Action creators

export function changeListState(currentState) {
  return {
    type: CHANGE_LIST,
    payload: currentState
  }
}